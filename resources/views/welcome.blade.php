
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eberg</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<!-- <body>
    <header>
        <nav class="navbar navbar-expand-sm fixed-top" style="background-color: #FF0000;">
            <div class="container-fluid">
                <a class="navbar-brand ms-5" href="{{ url('/') }}"><img src="{{ Vite::asset('resources/images/WhiteLetters.png') }}" width="300" height="80"></a>
                <div id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-light active fs-5 font-monospace" href="{{ url('/') }}">Home</a> 
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-light fs-5 ms-3 font-monospace" href="{{ url('/services') }}">Our Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-light fs-5 ms-3 font-monospace" href="{{ url('/workform') }}">Workform</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-light fs-5 ms-3 font-monospace" href="{{ url('/contactus') }}">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main class="py-1">
        @yield('content')
    </main>

    <script src="{{ asset('js/app.js') }}"></script>
</body> -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color: #FF0000;">
		<div class="container">
			<a class="navbar-brand" href="#"><img src="{{ Vite::asset('resources/images/WhiteLetters.png') }}" width="300" height="80"></a><button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-bs-target="#navbarSupportedContent" data-bs-toggle="collapse" type="button"><span class="navbar-toggler-icon"></span></button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link fs-5 text-light font-monospace fw-semibold" href="#">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link fs-5 text-dark font-monospace fw-semibold" href="#about">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link fs-5 text-light font-monospace fw-semibold" href="#services">Our Services</a>
					</li>
					<li class="nav-item">
						<a class="nav-link fs-5 text-dark font-monospace fw-semibold" href="#workform">Workform</a>
					</li>
					<li class="nav-item">
						<a class="nav-link fs-5 text-light font-monospace fw-semibold" href="#contact">Contact Us</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="carousel slide" data-bs-ride="carousel" id="carouselExampleIndicators">
		<div class="carousel-indicators">
			<button aria-label="Slide 1" class="active" data-bs-slide-to="0" data-bs-target="#carouselExampleIndicators" type="button"></button> 
            <button aria-label="Slide 2" data-bs-slide-to="1" data-bs-target="#carouselExampleIndicators" type="button"></button> 
            <button aria-label="Slide 3" data-bs-slide-to="2" data-bs-target="#carouselExampleIndicators" type="button"></button>
		</div>
		<div class="carousel-inner" >
			<div class="carousel-item active" style="height: 550px">
				<img src="{{ Vite::asset('resources/images/webdev3.jpg') }}" alt="..." class="d-block w-100">
				<div class="carousel-caption">
					<p class="d-flex justify-content-center display-3 fw-bold ">EBERG BUSINESS</p>
					<p>The business process automation specialist</p>
					<p><a class="btn btn-warning" href="#about">Learn More</a></p>
				</div>
			</div>
			<div class="carousel-item" style="height: 550px">
				<img src="{{ Vite::asset('resources/images/webdev2.jpg') }}" alt="..." class="d-block w-100">
				<div class="carousel-caption">
					<p class="d-flex justify-content-center display-3 fw-bold">Our Services</p>
					<p>We offer variety of services to help develop website or application.</p>
					<p><a class="btn btn-warning" href="#services">Learn More</a></p>
				</div>
			</div>
			<div class="carousel-item" style="height: 550px">
				<img src="{{ Vite::asset('resources/images/appdev.jpg') }}" alt="..." class="d-block w-100">
				<div class="carousel-caption">
					<p class="d-flex justify-content-center display-3 fw-bold">Workform</p>
					<p>An innovative platform to manage your business process</p>
					<p><a class="btn btn-warning" href="#workform">Learn More</a></p>
				</div>
			</div>
		</div><button class="carousel-control-prev" data-bs-slide="prev" data-bs-target="#carouselExampleIndicators" type="button"><span aria-hidden="true" class="carousel-control-prev-icon"></span> <span class="visually-hidden">Previous</span></button> <button class="carousel-control-next" data-bs-slide="next" data-bs-target="#carouselExampleIndicators" type="button"><span aria-hidden="true" class="carousel-control-next-icon"></span> <span class="visually-hidden">Next</span></button>
	</div><!-- about section starts -->
	<section class="about section-padding" id="about">
		<div class="lh-1">
    		<div class="d-flex justify-content-center mt-4">Welcome To</div>
    		<div class="d-flex justify-content-center display-1 fw-bold text-black">EBERG BUSINESS</div>
    		<div class="d-flex justify-content-center fs-5 text-danger fw-semibold">The business process automation specialist</div>
		</div>
		<div class="d-flex justify-content-center display-6 fw-bold text-black mt-5">What We Do ?</div>
		<div class="d-flex justify-content-center">		
			<div class="column mt-3">
				<div class="col-12 col-lg-12">
					<div class="card text-center">
						<div class="card-body">
							<p class="card-text">&#x2022; <span class="text-danger fw-bold">Help </span> businesses to automate their current manual and tedious work process.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-12 mt-2">
					<div class="card text-center">
						<div class="card-body">
							<p class="card-text">&#x2022; To <span class="text-danger fw-bold">improve </span> efficiency and effectiveness.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-12 mt-2">
					<div class="card text-center">
						<div class="card-body">
							<p class="card-text">&#x2022; <span class="text-danger fw-bold">Reduce </span> your company monthly overhead.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-12 mt-2">
					<div class="card text-center">
						<div class="card-body">
							<p class="card-text">&#x2022; <span class="text-danger fw-bold">Implement </span> SOP to your business process and make it more systematic.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-12 mt-2">
					<div class="card text-center">
						<div class="card-body">
							<p class="card-text">&#x2022; <span class="text-danger fw-bold">Provide </span> insight on business unit performance.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- about section Ends -->
	<!-- services section Starts -->
	<section class="services section-padding" id="services">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-header text-center pb-5 mt-5">
						<p class="fs-1 fw-bold mt-5">Our Services</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="card text-white text-center bg-dark mt-2">
						<div class="card-body">
							<img src="{{ Vite::asset('resources/images/webappdev.png') }}" alt="..." class="d-block w-100" style="height: 250px">
							<p class="fs-4 fw-bold mt-2">Web Application Development</p>
							<p><a class="btn btn-warning" href="#contact">Contact Us Now</a></p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card text-white text-center bg-dark mt-2">
						<div class="card-body">
							<img src="{{ Vite::asset('resources/images/mobileappdev.jpg') }}" alt="..." class="d-block w-100" style="height: 250px">
							<p class="fs-4 fw-bold mt-2">Mobile App Development</p>
							<p><a class="btn btn-warning" href="#contact">Contact Us Now</a></p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card text-white text-center bg-dark mt-2">
						<div class="card-body" style="height: 430px">
							<img src="{{ Vite::asset('resources/images/email.jpg') }}" alt="..." class="d-block w-100" style="height: 250px">
							<p class="fs-4 fw-bold mt-2">Email Support</p>
							<p><a class="btn btn-warning" href="#contact">Contact Us Now</a></p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card text-white text-center bg-dark mt-2">
						<div class="card-body"  style="height: 430px">
							<img src="{{ Vite::asset('resources/images/appsupport.png') }}" alt="..." class="d-block w-100" style="height: 250px">
							<p class="fs-4 fw-bold mt-2">Application support</p>
							<p><a class="btn btn-warning" href="#contact">Contact Us Now</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- services section Ends -->
	<!-- workform starts -->
	<section class="workform section-padding" id="workform">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-header text-center pb-5 mt-5">
						<p class="fs-1 fw-bold mt-5">Eberg Workform</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6 col-lg-3">
					<div class="card text-center">
						<div class="card-body">
							<h4 class="card-text">An innovative platform to <span class="text-danger fw-bold">manage </span> your business process.</h4>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-3">
					<div class="card text-center">
						<div class="card-body">
							<h4 class="card-text">No <span class="text-danger fw-bold">coding required </span>.</h4>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-3">
					<div class="card text-center">
						<div class="card-body">
							<h4 class="card-text">Drag drop to <span class="text-danger fw-bold">build </span>your form.</h4>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-3">
					<div class="card text-center">
						<div class="card-body">
							<h4 class="card-text"><span class="text-danger fw-bold">Powerful workflow </span>editor allow you to define the business process of the form request.</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- workform ends -->
	<!-- Contact starts -->
	<section class="contact section-padding" id="contact">
		<div class="container mt-5 mb-5">
			<div class="row">
				<div class="col-md-12">
					<div class="section-header text-center">
						<p class="fs-1 fw-bold">Contact Us</p>
					</div>
					<div class="input-group">
  						<input type="text" class="form-control" placeholder="Name" aria-label="name">
					</div>
					<div class="input-group mt-3">
  						<input type="text" class="form-control" placeholder="Email" aria-label="email">
					</div>
					<div class="input-group mt-3">
  						<textarea class="form-control" aria-label="message" placeholder="Messages"></textarea>
					</div>
				</div>
				<div class="position-relative mt-3">
				<p><a class="btn btn-warning position-absolute top-100 start-50 translate-middle" href="#services">Contact Us Now</a></p>
				</div>
			</div>
		</div>
	</section>

	<footer class="bg-dark p-2 text-center">
		<div class="container">
			<p class="text-white">All Right Reserved By Eberg Solution</p>
		</div>
	</footer>
	
	<script src="js/bootstrap.bundle.min.js"></script> 
</body>
</html>

