<?php
  
namespace App\Http\Controllers;
  
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Models\Contact;
  
class ContactController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        return view('contactForm');
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        // print_r($request->all());
        // exit();

        $client = new Client([
            'base_uri' => 'https://www.google.com',
            'verify' => false,
        ]);

        $response = $client->post('recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => env('RECAPTCHA_SECRET'),
                'response' => $request->input('g-recaptcha-response'),
                'remoteip' => $request->ip(),
            ]
        ]);

        $body = $response->getBody();

        $data = json_decode($body, true);

        if (Arr::get($data, 'score', 0) < 0.5) {
            // throw error.
            // return response();
        }
  
        Contact::create($request->all());
        
        return redirect()->back()
                         ->with(['success' => 'Thank you for contact us. we will contact you shortly.']);
    }

    // public function test()
    // {
    //     $data = [
    //         'foo' => [
    //             'nested' => [
    //                 'value' => 'Here.'
    //             ]
    //         ]
    //     ];

    //     // echo $data['foos'];
    //     echo Arr::get($data, 'foo.nested.value', 'unknown');
    // }
}